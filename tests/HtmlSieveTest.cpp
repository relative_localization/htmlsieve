﻿#include <taparser/HtmlReader.h>

#include <gtest/gtest.h>

using namespace taparser;

TEST(HtmlSieveTest, withUrl) {
  std::string content =
      HtmlReader::getHtmlContent("https://www.tripadvisor.com");
  EXPECT_NE("", content);
  EXPECT_LT(0,content.size());
}

TEST(HtmlSieveTest, notFound) {
  std::string content =
      HtmlReader::getHtmlContent("https://www.NEtripadvisor.com/");
  EXPECT_EQ("", content);
}