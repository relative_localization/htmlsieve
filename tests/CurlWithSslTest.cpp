#include <curl/curl.h>
#include <gtest/gtest.h>

TEST(CheckSslTest, isEnable) {
curl_version_info_data *curl_version_info( CURLversion age );
auto data = curl_version_info (CURLVERSION_NOW);
std::string version = (data->ssl_version==NULL) ? "" :
                      std::string(data->ssl_version);
ASSERT_EQ(false,version.empty()) << "Curl version without ssl";

}

