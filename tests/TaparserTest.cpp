#include <taparser/Taparser.h>

#include <gtest/gtest.h>

#include <chrono>
#include <filesystem>
#include <fstream>
#include <string>

using namespace taparser;

std::string getDataFilePath() {
  const auto sourceDir = std::filesystem::path(SOURCE_DIR);
  return (sourceDir / "data/LinkForTests.txt").string();
}

void printResult(const std::map<std::string, std::string> &result) {
  if (result.empty())
    std::cout << "Empty\n";
  for (const auto &field : result) {
    std::cout << field.first << ": " << field.second << std::endl;
  }
}

TEST(TaparserTest, DISABLED_Stability) {
  std::ifstream file(getDataFilePath());
  std::string url;
  int success = 0;
  int failure = 0;
  std::map<Field, int> nFields = {
      {"Price", 0},   {"Name", 0},   {"Type", 0},
      {"Image", 0},   {"Rating", 0}, {"Street", 0},
      {"Website", 0}, {"Phone", 0},  {"Subtype", 0},
  };
  auto begin = std::chrono::high_resolution_clock::now();
  while (getline(file, url)) {
    auto ta = taparser::Taparser(url);
    ta.parse();
    auto results = ta.getTaFields();
    if (results.empty()) {
      failure++;
    } else {
      for (auto &field : nFields)
        if (results.find(field.first) != results.end())
          nFields[field.first] = field.second + 1;
      success++;
    }
  }
  auto end = std::chrono::high_resolution_clock::now();

  double percent;
  if (failure != 0) {
    percent = success * 100 / (failure + success);
  } else {
    percent = 100;
  }
  std::cout << "success/failure: ";
  std::cout << success << "/" << failure << " - " << percent << "%\n";
  for (auto &field : nFields)
    std::cout << field.first << " : " << field.second << "\n";
  std::cout << "Spent on processing " << success + failure << " pages: ";

  std::cout << double(std::chrono::duration_cast<std::chrono::microseconds>(
                          end - begin)
                          .count() /
                      1000000.0)
            << "s\n";

  EXPECT_GE(percent, 85.0);
}

TEST(TaparserTest, DISABLED_taOnlineFruktovayaLavka) {
  std::string url = "https://www.tripadvisor.com/"
                    "Restaurant_Review-g298507-d8525599-Reviews-Fruktovaya_"
                    "Lavka-St_Petersburg_"
                    "Northwestern_District.html";

  auto ta = Taparser(url);
  ta.parse();
  auto fields = ta.getTaFields();
  printResult(fields);
  EXPECT_GT(fields.size(), 5);
  EXPECT_NE(fields["Name"].find("Fruktovaya Lavka"), std::string::npos);
  EXPECT_EQ(fields["Type"], "restaurant");
  if (fields["Name"].find("- CLOSED") == std::string::npos) {
    EXPECT_GT(atof(fields["Rating"].c_str()), 0);
    EXPECT_GT(atoi(fields["nReviews"].c_str()), 0);
  }
}

TEST(TaparserTest, DISABLED_taOnlineRomansCafe) {
  const std::string url =
      "https://www.tripadvisor.com/"
      "Restaurant_Review-g1207894-d6117654-Reviews-Romans_Cafe-Peterhof_"
      "Petrodvortsovy_District_St_Petersburg_Northwestern_District.html";

  auto ta = Taparser(url);
  ta.parse();
  auto fields = ta.getTaFields();
  printResult(fields);
  EXPECT_GT(fields.size(), 5);
  EXPECT_EQ(fields["Name"], "Romance-Cafe");
  EXPECT_EQ(fields["Type"], "restaurant");
  EXPECT_GT(atof(fields["Rating"].c_str()), 0);
  EXPECT_GT(atoi(fields["nReviews"].c_str()), 0);
}

TEST(TaparserTest, DISABLED_taOnlineRomansCafeRu) {
  const std::string url =
      "https://www.tripadvisor.com/"
      "Restaurant_Review-g1207894-d6117654-Reviews-Romans_Cafe-Peterhof_"
      "Petrodvortsovy_District_St_Petersburg_Northwestern_District.html";

  auto ta = Taparser(url, "ru");
  ta.parse();
  auto fields = ta.getTaFields();
  EXPECT_EQ(fields["Name"], "Романс-Кафе");
}

TEST(TaparserTest, DISABLED_taOnlineSimplyThaiCn) {
  const std::string url =
      "https://www.tripadvisor.ru/Restaurant_Review-"
      "g308272-d924297-Reviews-Simply_Thai_Xin_Tian_Di-Shanghai.html";

  auto ta = Taparser(url, "cn");
  ta.parse();
  auto fields = ta.getTaFields();
  printResult(fields);
  EXPECT_EQ(fields["Name"], "simplythai天泰餐厅(新天地店)");
  EXPECT_EQ(fields["Street"], "黄浦区马当路159号");
}

TEST(TaparserTest, DISABLED_taOnlineSpazioMurat) {
  std::string url = "https://www.tripadvisor.it/"
                    "Attraction_Review-g187874-d12109637-Reviews-Spazio_Murat-"
                    "Bari_Province_of_Bari_Puglia.html";

  auto ta = Taparser(url);
  ta.parse();
  auto fields = ta.getTaFields();
  printResult(fields);
  EXPECT_GT(fields.size(), 5);
  EXPECT_EQ(fields["Name"], "Spazio Murat");
  EXPECT_EQ(fields["Type"], "place");
  EXPECT_GT(atof(fields["Rating"].c_str()), 0);
  EXPECT_GT(atoi(fields["nReviews"].c_str()), 0);
}

TEST(TaparserTest, DISABLED_taOnlineHotelPiccadilly) {
  std::string url =
      "https://www.tripadvisor.ru/Hotel_Review-g186338-"
      "d6703394-Reviews-The_Z_Hotel_Piccadilly-London_England.html";

  auto ta = Taparser(url);
  ta.parse();
  auto fields = ta.getTaFields();
  printResult(fields);
  EXPECT_GT(fields.size(), 5);
  EXPECT_EQ(fields["Name"], "The Z Hotel Piccadilly");
  EXPECT_EQ(fields["Type"], "other");
  EXPECT_GT(atof(fields["Rating"].c_str()), 0);
  EXPECT_GT(atoi(fields["nReviews"].c_str()), 0);
}

TEST(TaparserTest, DISABLED_taOnlineNyhavnsGlaspusteri) {
  std::string url =
      "https://www.tripadvisor.com/Attraction_Review-g189541-"
      "d10210337-Reviews-Nyhavns_Glaspusteri-Copenhagen_Zealand.html";

  auto ta = Taparser(url);
  ta.parse();
  auto fields = ta.getTaFields();
  printResult(fields);
  EXPECT_GT(fields.size(), 5);
  EXPECT_EQ(fields["Name"], "Nyhavns Glaspusteri");
  EXPECT_EQ(fields["Type"], "shop");
  EXPECT_GT(atof(fields["Rating"].c_str()), 0);
  EXPECT_GT(atoi(fields["nReviews"].c_str()), 0);
}
