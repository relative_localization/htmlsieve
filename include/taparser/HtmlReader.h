#ifndef HTML_READER_H
#define HTML_READER_H

#include <string>

namespace taparser {

/** Class to download content from the site by url.
 */

class HtmlReader {

public:
  HtmlReader(const std::string &url);

  ~HtmlReader();

  void parse();
  const std::string &htmlContent() const;

  // Get full html content by url
  static std::string getHtmlContent(const std::string &url);

protected:
  std::string html_content;
  std::string url;

private:
  // To read content by url
  static size_t writeData(char *ptr, size_t size, size_t nmemb,
                          std::string *data);

  bool status_init = false;
};

} // namespace taparser

#endif // HTML_READER_H
