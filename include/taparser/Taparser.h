#ifndef TAPARSER_TAPARSER_H
#define TAPARSER_TAPARSER_H

#include <taparser/HtmlReader.h>

#include <nlohmann/json.hpp>

#include <map>
#include <optional>
#include <string>
#include <vector>

namespace taparser {

using Field = std::string; // Field in the site
using SearchKeys =
    std::map<std::string,
             std::string>;      // Search key (for example: div.address)
using FieldValue = std::string; // Found field value in the site
using FieldValues = std::vector<std::string>; // Found field values in the site
using Language = std::string; // Estimated language for found fields
using Domain = std::string;
using TaType = std::string;
using DoorsType = std::string;

/** Class to search for fields in the site tripadvisor.
 *  Supports add your pairs field - search key to the vocabulary:
 *      for example:
 *      {
 *          // Field         // Search keys
 *          {"Phone",       {"\"phone\":\"", "\""}},
 *          {"Website",     {"\"website\":\"", "\""}}
 *      }
 *  When create class object is formed default vocabulary.
 *  Additional vocabulary overwrites the default vocabulary.
 *  getTaFields()   - all found fields from the vocabulary
 */
class Taparser : public taparser::HtmlReader {
public:
  Taparser(const std::string &url, const Language &language = "default",
           const std::map<Field, SearchKeys> &additional_vocabulary = {{}});

  const std::map<Field, FieldValue> &getTaFields();
  static bool updateTripadvisorUrl(std::string &url,
                                   const Language &language = "default");

private:
  // Preprocessing
  bool parseJson();
  std::string getCssContent() const;
  bool getJsonContent();

  FieldValues getSubstrings(const std::string &start,
                            const std::string &end) const;

  static Domain domain(const Language &language);

  static bool replaceString(std::string &str, const std::string &from,
                            const std::string &to);

  static bool isPhone(const std::string &str);

  static void checkWebsite(std::map<Field, FieldValues> &fields_values);

  static void checkPhone(std::map<Field, FieldValues> &fields_values);

  static void checkSubtype(std::map<Field, FieldValue> &result);
  static void checkShop(std::map<Field, FieldValue> &result);

  static void deleteNotFoundFields(std::map<Field, FieldValues> &fields_values);

  static void deleteEmptyFields(std::map<Field, FieldValues> &fields_values);

  static std::string clearPrice(const std::string &price);
  static std::string replaceType(const std::string &type);

  std::optional<nlohmann::json> json_data;

  std::map<Field, SearchKeys> vocabulary;
  std::map<Field, FieldValue> result;
  bool status_init = false;
};

} // namespace taparser

#endif // TAPARSER_TAPARSER_H
