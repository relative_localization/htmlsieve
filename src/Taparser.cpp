#include <taparser/HtmlReader.h>
#include <taparser/Taparser.h>

#include <glog/logging.h>

#include <map>
#include <regex>
#include <string>

namespace taparser {

using json = nlohmann::json;

static const std::map<Field, SearchKeys> css_vocabulary = {
    {"Phone", {{"\"phone\":\"", "\""}, {"\"phone\" : \"", "\""}}},
    {"Website",
     {
         {"\"website\":\"", "\""},
         {"\"website\" : \"", "\""},
     }},
    {"Subtype",
     {
         {"<div class=\"_28X3NMFC\"><div class=\"DrjyGw-P _26S7gyB4 "
          "_2nPM5Opx\">",
          "<"},
     }},
};

static const std::map<Language, Domain> language2domain_map = {
    {"en", "com"},
    {"it", "it"},
    {"ru", "ru"},
    {"cn", "cn"},
    {"default", "com"}};

static const std::map<TaType, DoorsType> taType2doorsType_map = {
    {"LocalBusiness", "place"},
    {"FoodEstablishment", "restaurant"},
    {"LodgingBusiness", "other"},
    {"default", "other"},
};

bool isJsonField(const json &obj, const std::string &key) {
  return obj.find(key) != obj.end();
}

Taparser::Taparser(const std::string &url, const Language &language,
                   const std::map<Field, SearchKeys> &additional_vocabulary)
    : HtmlReader(url) {
  this->vocabulary = css_vocabulary;
  for (const auto &field : additional_vocabulary) {
    this->vocabulary[field.first] = field.second;
  }
  updateTripadvisorUrl(this->url, language);
}

bool Taparser::updateTripadvisorUrl(std::string &url,
                                    const Language &language) {
  if (url.find("tripadvisor.") == std::string::npos) {
    return false;
  }

  url = std::regex_replace(url, std::regex("tripadvisor.[a-z.]+/"),
                           "tripadvisor." + domain(language) + "/");

  replaceString(url, "LocationPhotoDirectLink", "Restaurant_Review");
  replaceString(url, "ShowUserReviews", "Restaurant_Review");
  return true;
}

Domain Taparser::domain(const Language &language) {
  if (language2domain_map.find(language) != language2domain_map.end())
    return language2domain_map.at(language);
  return language2domain_map.at("default");
}

bool Taparser::replaceString(std::string &str, const std::string &from,
                             const std::string &to) {

  size_t start_pos = str.find(from);
  if (start_pos == std::string::npos) {
    return false;
  }

  str.replace(start_pos, from.length(), to);
  return true;
}

const std::map<Field, FieldValue> &Taparser::getTaFields() {

  if (status_init) {
    return result;
  }

  std::map<Field, FieldValues> fields_values;

  for (const auto &voc : vocabulary) {
    for (const auto &keys : voc.second)
      for (const auto &value : getSubstrings(keys.first, keys.second))
        fields_values[voc.first].push_back(value);
  }
  checkWebsite(fields_values);
  checkPhone(fields_values);
  deleteNotFoundFields(fields_values);
  getJsonContent();

  parseJson();
  for (const auto &field : fields_values) {
    if (!field.second.empty())
      result[field.first] = field.second[0];
    else {
      result[field.first] = "";
      LOG(WARNING) << "Field values for " << field.first << " is empty";
    }
  }

  checkSubtype(result);
  checkShop(result);
  status_init = true;
  return result;
}

void Taparser::deleteNotFoundFields(
    std::map<taparser::Field, taparser::FieldValues> &fields_values) {

  for (const auto &field : fields_values) {
    for (auto it = field.second.begin(); it != field.second.end();) {
      if (it->find("not found") != std::string::npos) {
        it = fields_values[field.first].erase(it);
      } else {
        it++;
      }
    }
  }

  deleteEmptyFields(fields_values);
}

void Taparser::deleteEmptyFields(std::map<Field, FieldValues> &fields_values) {

  for (auto it = fields_values.begin(); it != fields_values.end();) {
    if ((*it).second.empty()) {
      it = fields_values.erase(it);
    } else {
      it++;
    }
  }
}

bool Taparser::parseJson() {
  if (!json_data)
    return false;
  auto add_field = [this](auto &result, const std::string &out,
                          const std::vector<std::string> &in) {
    auto next_item = *json_data;
    for (const auto &i : in) {
      if (isJsonField(next_item, i)) {
        next_item = next_item[i];
      } else
        return;
    }
    if (next_item.is_string())
      next_item.get_to(result[out]);
    else if (next_item.is_number()) {
      if (next_item.is_number_integer())
        result[out] = std::to_string(next_item.get<int>());
      else
        result[out] = std::to_string(next_item.get<double>());
    } else
      LOG(WARNING) << "Unsupported field type: " << next_item;
  };

  add_field(result, "Price", {"priceRange"});
  add_field(result, "Name", {"name"});
  add_field(result, "Type", {"@type"});
  add_field(result, "Image", {"image"});
  add_field(result, "nReviews", {"aggregateRating", "reviewCount"});
  add_field(result, "Rating", {"aggregateRating", "ratingValue"});
  add_field(result, "Street", {"address", "streetAddress"});

  if (result.find("Price") != result.end()) {
    result["Price"] = clearPrice(result["Price"]);
  }

  if (result.find("Type") != result.end()) {
    result["Type"] = replaceType(result["Type"]);
  }

  return true;
}

std::string Taparser::clearPrice(const std::string &price) {
  std::string only_price;
  for (const auto &ch : price) {
    if (ch == '$' || ch == ' ' || ch == '-') {
      only_price.push_back(ch);
    }
  }
  return only_price;
}

std::string Taparser::replaceType(const std::string &type) {
  if (taType2doorsType_map.find(type) != taType2doorsType_map.end())
    return taType2doorsType_map.at(type);
  return taType2doorsType_map.at("default");
}

std::string Taparser::getCssContent() const {
  auto substrings =
      getSubstrings("window.__WEB_CONTEXT__=", "m.exports=__WEB_CONTEXT__");
  if (!substrings.empty())
    return substrings[0];
  return {};
}

bool Taparser::getJsonContent() {

  const auto items =
      getSubstrings("<script type=\"application/ld+json\">", "</script>");
  for (auto &item : items) {
    VLOG(2) << "TA Json: " << item;
    if (item.empty())
      continue;
    if (!json::accept(item))
      continue;

    auto json_item = json::parse(item);
    if (isJsonField(json_item, "name") && isJsonField(json_item, "address")) {
      json_data = std::move(json_item);
      return true;
    }
  }

  return false;
}

FieldValues Taparser::getSubstrings(const std::string &start,
                                    const std::string &end) const {
  FieldValues result;
  size_t pos = html_content.find(start);
  std::vector<size_t> all_start_pos;
  while (pos != std::string::npos) {
    all_start_pos.push_back(pos + start.length());
    pos = html_content.find(start, pos + start.length());
  }

  for (const auto &start_pos : all_start_pos) {
    size_t end_pos = html_content.find(end, start_pos + start.length());
    size_t length_substr = end_pos - start_pos;
    result.push_back(html_content.substr(start_pos, length_substr));
  }

  if (result.empty())
    result.push_back("");
  return result;
}

void Taparser::checkWebsite(std::map<Field, FieldValues> &fields_values) {

  if (fields_values.find("Website") == fields_values.end()) {
    return;
  }

  if (fields_values["Website"].empty()) {
    return;
  }

  for (auto it = fields_values["Website"].begin();
       it != fields_values["Website"].end();) {
    if (it->find("www.") == std::string::npos &&
        it->find("http") == std::string::npos) {
      it = fields_values["Website"].erase(it);
    } else {
      it++;
    }
  }
}

void Taparser::checkPhone(std::map<Field, FieldValues> &fields_values) {

  if (fields_values.find("Phone") == fields_values.end()) {
    return;
  }

  if (fields_values["Phone"].empty()) {
    return;
  }

  for (auto it = fields_values["Phone"].begin();
       it != fields_values["Phone"].end();) {
    if (!isPhone((*it))) {
      it = fields_values["Phone"].erase(it);
    } else {
      it++;
    }
  }
}

void Taparser::checkSubtype(std::map<Field, FieldValue> &result) {
  if (result.find("Subtype") != result.end())
    result["Subtype"] =
        result["Subtype"].substr(0, result["Subtype"].find_first_of("<"));
  if (result["Subtype"].empty())
    result.erase("Subtype");
}

void Taparser::checkShop(std::map<Field, FieldValue> &result) {
  if (result.find("Subtype") == result.end())
    return;
  if (result["Subtype"].find("Shop") != std::string::npos)
    result["Type"] = "shop";
}

bool Taparser::isPhone(const std::string &str) {
  std::string valid_characters = "+1 (234) 567-890";
  if (str.empty())
    return false;
  for (auto &ch : str) {
    if (valid_characters.find(ch) == std::string::npos) {
      return false;
    }
  }
  return true;
}

} // namespace taparser
