#include <taparser/HtmlReader.h>

#include <curl/curl.h>

#include <string>
#include <vector>

namespace taparser {

HtmlReader::HtmlReader(
    const std::string &url)
    : url(url){}

HtmlReader::~HtmlReader() = default;

std::string HtmlReader::getHtmlContent(const std::string &url) {
  CURL *curlHandle = curl_easy_init();
  std::string content;
  if (curlHandle) {
    curl_easy_setopt(curlHandle, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_USERAGENT, "Mozilla/5.0");
    curl_easy_setopt(curlHandle, CURLOPT_DEFAULT_PROTOCOL, "https");
    curl_easy_setopt(curlHandle, CURLOPT_EXPECT_100_TIMEOUT_MS, 3000L);
    curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYPEER, false);
    curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, false);
    curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curlHandle, CURLOPT_AUTOREFERER, 1);
    curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, writeData);
    curl_easy_setopt(curlHandle, CURLOPT_SSL_CIPHER_LIST, "DEFAULT:!DH");
    curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &content);
    curl_easy_perform(curlHandle);
    curl_easy_cleanup(curlHandle);
  }
  return content;
}

size_t HtmlReader::writeData(char *ptr, size_t size, size_t nmemb,
                            std::string *data) {
  if (data) {
    data->append(ptr, size * nmemb);
    return size * nmemb;
  }
  return 0;
}

void HtmlReader::parse() {
  if (!status_init) {
    html_content = getHtmlContent(url);
  }
  status_init = true;
}

const std::string &HtmlReader::htmlContent() const {
  return html_content;
}

} // namespace taparser