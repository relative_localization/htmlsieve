#!/bin/bash
mkdir -p build/dependencies/openssl
mkdir -p prefix
BASEDIR=$(dirname $(realpath "$0"))
PREFIX=`pwd`/prefix
cd build/dependencies/openssl
$BASEDIR/dependencies/openssl/config --prefix=$PREFIX
make
make install prefix=$PREFIX
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PREFIX/lib
cd $BASEDIR/dependencies/curl
if [ ! -e configure ]; then
	if [ -e buildconf ]; then
		./buildconf
	fi
fi
./configure --with-ssl=$PREFIX --prefix=$PREFIX
make 
make install prefix=$PREFIXcd 
cd $BASEDIR/build
cmake -DCMAKE_PREFIX_PATH:PATH=$PREFIX -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX ..
cmake --build . --target install
cd tests
./taparser_tests

