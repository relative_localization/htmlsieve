# Installation

mkdir build && cd build

cmake -DCMAKE_INSTALL_PREFIX:PATH=/path/to/build -DCMAKE_PREFIX_PATH:PATH=/path/to/build -DCMAKE_BUILD_TYPE=Release ..

**Add for windows** :  -DCMAKE_USE_WINSSL=ON -DWIN32=ON 

cmake --build . --target install 

